package serialization.basic;

import com.fasterxml.jackson.annotation.JsonValue;

public enum UserValueEnum {

  FIRST_NAME("Michael"),
  LAST_NAME("Jackson");

  private String name;

  UserValueEnum(String name) {
    this.name = name;
  }

  @JsonValue
  public String getName() {
    return name;
  }
}
