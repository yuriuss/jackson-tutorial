package serialization.basic;

public class UserBean {

  public int id;
  private String firstName;

  public UserBean() {
  }

  public UserBean(int id, String firstName) {
    this.id = id;
    this.firstName = firstName;
  }

  public String getVersion() {
    return "1.0";
  }

  public String getFirstName() {
    return firstName;
  }
}
