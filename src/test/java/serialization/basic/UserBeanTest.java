package serialization.basic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class UserBeanTest {

  @Test
  public void testUserBeanForModifiers() throws JsonProcessingException {
    UserBean user = new UserBean(1, "John");

    String resultJson = new ObjectMapper().writeValueAsString(user);
    System.out.println(resultJson);

    assertThat(resultJson, containsString("id"));
    assertThat(resultJson, containsString("firstName"));
  }

}