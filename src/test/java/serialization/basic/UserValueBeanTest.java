package serialization.basic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class UserValueBeanTest {

  @Test
  public void testUserValueBean() throws JsonProcessingException {
    UserValueBean userValueBean = new UserValueBean(1, "Michael");

    String jsonResult = new ObjectMapper().writeValueAsString(userValueBean);
    System.out.println(jsonResult);

    assertThat(jsonResult, containsString("UserValueBean"));
  }

}